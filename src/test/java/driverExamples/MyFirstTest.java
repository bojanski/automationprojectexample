package driverExamples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MyFirstTest {

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        System.setProperty("webdriver.gecko.driver", "C:\\webdrivers\\geckodriver.exe");
        this.driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        this.driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public void myFirstTest() {
        WebElement usernameField = this.driver.findElement(By.cssSelector("#search input"));
        usernameField.sendKeys("dqdo koleda");
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        this.driver.quit();
    }

}
