package driverExamples;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class InternetExplorerDriverExampleTest {
	WebDriver driver;

	@BeforeMethod
	public void setUp(){
		System.setProperty("webdriver.ie.driver", "C:\\webdrivers\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://pragmatic.bg/automation/example4.html");
	}

	@Test
	public void testExamples() {
		WebElement element = driver.findElement(By.id("dateInput"));
		element.sendKeys("100");
	}
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	
}
