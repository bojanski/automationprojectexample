package simpleBrowserExamples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SimpleBrowserExamples {

    private static WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
//
//        System.setProperty("webdriver.gecko.driver", "C:\\webdrivers\\geckodriver.exe");
//        driver = new FirefoxDriver();
//
//        This example is using OperaOptions and setBinary file due to error in case the binary is not set by
//        default for some reason
//
//        System.setProperty("webdriver.opera.driver","C:\\webdrivers\\operadriver_win64\\operadriver.exe");
//        OperaOptions options = new OperaOptions();
//        options.setBinary("C:\\Users\\GeorgiTsvetanov\\AppData\\Local\\Programs\\Opera\\66.0.3515.27\\opera.exe");
//        driver = new OperaDriver(options);
//
//        System.setProperty("webdriver.edge.driver", "C:\\webdrivers\\MicrosoftWebDriver.exe");
//        driver = new EdgeDriver();
//        !!! IMPORTANT NOTE: In case you are getting binary not found error - check how to set the binary presented in the EdgeDriverExample !!!
//
//        System.setProperty("webdriver.ie.driver", "C:\\webdrivers\\IEDriverServer.exe");
//        driver = new InternetExplorerDriver();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public void testOne() {
        WebElement usernameInput = driver.findElement(By.cssSelector("#search input"));
        usernameInput.sendKeys("pragmabg");
    }

    @AfterMethod
    public void closeBrowser() {
        driver.quit();
    }
}
